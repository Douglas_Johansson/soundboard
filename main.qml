import QtQuick 2.12
import QtQuick.Window 2.12
import QtMultimedia
import QtQuick.Controls

Window {
    width: 1280
    height: 720
    visible: true
    title: qsTr("SoundBoard")



property var sounds: ["applause.wav", "boing.wav", "burp.wav", "cash.wav", "evillaugh",
        "falling.wav", "farm.wav", "fart.wav", "hello.wav", "laugh.wav", "snare.wav",
    "sonic.wav", "water.wav", "water2.wav"]

    Grid {
          id: grid
          anchors.centerIn:parent
          rows: 4; columns: 4; spacing: 0

          Repeater {
                model: 16

          AbstractButton {
            id: pushbutton
            width:150;height:150


            onPressedChanged: {
                if(pressed){
                background.color = "White"
                } else {
                    background.color = Qt.rgba(Math.random(),Math.random(),Math.random(),1);
                }

            }
            background:Rectangle{
                id: background
                border.width: 5
                radius: 5
                color: Qt.rgba(Math.random(),Math.random(),Math.random(),1);
            }

           contentItem:Label {
                text: "Press me"
                font.pixelSize: 30
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
            MediaPlayer {
                id: sound
                source: "sounds/"+sounds[index]
                audioOutput: AudioOutput{}
            }

            onClicked: {
                sound.stop()
                sound.play();


             }
          }
          }

    }
}





